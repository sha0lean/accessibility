import $ from "jquery";

$(function () {
  console.log("gl hf");

  const openBtn = document.getElementById("modal-button");
  const closeBtn = document.getElementsByClassName("base-modal__close")[0];
  const modal = document.getElementById("address-modal");
  const firstInput = document.getElementById("firstinput");
  const lastInput = document.getElementById("lastinput");

  const toggleModal = function () {
    if (modal.classList.contains("base-modal--show")) {
      modal.classList.remove("base-modal--show");
      openBtn.focus();
    } else {
      modal.classList.add("base-modal--show");
      firstInput.focus();
    }
  };

  // backdrop ?
  openBtn.addEventListener("click", function (event) {
    event.stopPropagation();
    toggleModal();
  });

  closeBtn.addEventListener("click", function (event) {
    event.stopPropagation();
    toggleModal();
  });

  // focus trap
  modal.addEventListener("keydown", function (event) {
    console.log("1");
    if (event.key === "Tab") {
      console.log("2");
      if (event.shiftKey) {
        if (document.activeElement === closeBtn) {
          lastInput.focus();
          event.preventDefault();
        }
      } else {
        if (document.activeElement === lastInput) {
          console.log("3");
          closeBtn.focus();
          event.preventDefault();
        }
      }
    }
  });

  $(document).on("keydown", function (event) {
    if (event.key == "Escape") {
      closeBtn.click();
    }
  });
});

// alt maj F : format
